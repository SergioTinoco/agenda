/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import static com.sergioTinoco.agenda.CensoHermanos.hermanoSeleccionado;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author sergio
 */
public class Pantalla0Controller implements Initializable {

    private static final Logger LOG = Logger.getLogger(Pantalla0Controller.class.getName());

    private TableView<Hermano> table;
    private Hermano hermanoSeleccionado;
    @FXML
    private Button botonGuardar;
    @FXML
    private Button botonCancelar;
    private Hermano hermanoEditado;
    CensoHermanos objetoCensoHermanos;
    @FXML
    private TextField nombre;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField estadCivil;
    @FXML
    private TextField ciudadNacimiento;
    @FXML
    private TextField provinciaNacimiento;
    @FXML
    private TextField parroquia;
    @FXML
    private TextField ciudadParroquia;
    @FXML
    private TextField provinciaParroquia;
    @FXML
    private TextField dni;
    @FXML
    private TextField letraDni;
    @FXML
    private TextField profesion;
    @FXML
    private TextField telefono;
    @FXML
    private TextField domicilio;
    @FXML
    private TextField provinciaHermano;
    @FXML
    private ChoiceBox<?> tipoVia;
    @FXML
    private TextField nombreCalle;
    @FXML
    private TextField num;
    @FXML
    private TextField piso;
    @FXML
    private TextField codPostal;
    @FXML
    private TextField correo;
    @FXML
    private DatePicker fechIngreso;
    @FXML
    private Button botonEliminar;
    @FXML
    private Button botonDomiciliacion;
    @FXML
    private DatePicker fechIngreso1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setTableView(TableView<Hermano> tableView) {
        this.table = tableView;
    }

    public void showItemSelectedData() {
        // Obtener el objeto que está seleccionado en la tabla
        hermanoSeleccionado = table.getSelectionModel().getSelectedItem();
        nombre.setText(hermanoSeleccionado.getNombre());
        apellidos.setText(hermanoSeleccionado.getApellidos());
        estadCivil.setText(String.valueOf(hermanoSeleccionado.getEstadCivil()));
        dni.setText(hermanoSeleccionado.getDni());
        letraDni.setText(hermanoSeleccionado.getLetraDni());
        nombreCalle.setText(hermanoSeleccionado.getNombreCalle());
        codPostal.setText(hermanoSeleccionado.getCodPostal());
        domicilio.setText(hermanoSeleccionado.getDomicilio());
        telefono.setText(hermanoSeleccionado.getTelefono());
        profesion.setText(hermanoSeleccionado.getProfesion());
        //Fecha.setDateInDatePicker(fechNacimiento, hermanoSeleccionado.getFechNacimiento());
        ciudadNacimiento.setText(hermanoSeleccionado.getCiudadNacimento());
        provinciaNacimiento.setText(hermanoSeleccionado.getProvinciaNacimiento());
        parroquia.setText(hermanoSeleccionado.getParroquia());
        ciudadParroquia.setText(hermanoSeleccionado.getCiudadParroquia());
        provinciaParroquia.setText(hermanoSeleccionado.getProvinciaParroquia());
        provinciaHermano.setText(hermanoSeleccionado.getProvinciaHermano());
        num.setText(hermanoSeleccionado.getNum());
        piso.setText(hermanoSeleccionado.getPiso());
        correo.setText(hermanoSeleccionado.getEmail());
    }

    @FXML
    private void onMouseClickedBotonGuardar(MouseEvent event) {
        hermanoSeleccionado.setNombre(nombre.getText());
        hermanoSeleccionado.setApellidos(apellidos.getText());
        hermanoSeleccionado.setEstadCivil(estadCivil.getText());
        hermanoSeleccionado.setDni(dni.getText());
        hermanoSeleccionado.setLetraDni(letraDni.getText());
        hermanoSeleccionado.setNombreCalle(nombreCalle.getText());
        hermanoSeleccionado.setCodPostal(codPostal.getText());
        hermanoSeleccionado.setDomicilio(domicilio.getText());
        hermanoSeleccionado.setTelefono(telefono.getText());
        hermanoSeleccionado.setProfesion(profesion.getText());
        //Fecha.setDateInDatePicker(fechNacimiento,hermanoSeleccionado.getFechNacimiento());
        hermanoSeleccionado.setCiudadNacimento(ciudadNacimiento.getText());
        hermanoSeleccionado.setProvinciaNacimiento(provinciaNacimiento.getText());
        hermanoSeleccionado.setParroquia(parroquia.getText());
        hermanoSeleccionado.setCiudadParroquia(ciudadParroquia.getText());
        hermanoSeleccionado.setProvinciaParroquia(provinciaParroquia.getText());
        hermanoSeleccionado.setProvinciaHermano(provinciaHermano.getText());
        hermanoSeleccionado.setNum(num.getText());
        hermanoSeleccionado.setPiso(piso.getText());
        hermanoSeleccionado.setEmail(correo.getText());
        int indexItemSelected = table.getSelectionModel().getSelectedIndex();
        table.getItems().set(indexItemSelected, CensoHermanos.hermanoSeleccionado);
        int lastScreensNumber = CensoHermanos.root.getChildren().size() - 1;
        CensoHermanos.root.getChildren().remove(lastScreensNumber);
        LOG.finest("Se ha cerrado la ventana");
    }

    @FXML
    private void OnMouseClickedCancelar(MouseEvent event) {
    }

    public void setListaHermanos(CensoHermanos objetoCensoHermanos) {
        this.objetoCensoHermanos = objetoCensoHermanos;
    }

    @FXML
    private void onMouseClickedBotonEliminar(MouseEvent event) throws JAXBException {
        Alert alert = new Alert(AlertType.CONFIRMATION, "¿Quieres eliminar a esta persona?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            //Borramos el hermano 
            CensoHermanos.data.remove(CensoHermanos.hermanoSeleccionado);

            //Borramos el hermano contenido en la lista en el servidor
            JAXBContext jaxbContext = JAXBContext.newInstance(Hermano.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            try {
                // Crear una conexión con la URL del Servlet
                String strConnection = "http://localhost:8084/AplicacionWeb/Servlet";
                URL url = new URL(strConnection);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                // Esta conexión se va a hacer para  enviar y recibir información en formato XML
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-type", "text/xml");
                conn.setRequestMethod("DELETE");
                jaxbMarshaller.marshal(CensoHermanos.hermanoSeleccionado, conn.getOutputStream());
                //Ejecutar la conexión y obtener la respuesta
                InputStreamReader isr1 = new InputStreamReader(conn.getInputStream());
                LOG.fine("Peticion Delete: se ha borrado la persona");
                isr1.close();
            } catch (IOException ex) {
            }
        } else {
            LOG.fine("Presionado el botón Cancelar");
        }
    }

    }


