/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static javafx.application.Application.launch;
import javafx.scene.control.TableCell;
import javafx.scene.layout.AnchorPane;
import static javafx.application.Application.launch;

/**
 *
 * @author Sergio Tinoco
 */
public class CensoHermanos extends Application {

    private static final Logger LOG = Logger.getLogger(CensoHermanos.class.getName());
    private TableView<Hermano> table = new TableView<Hermano>();
    public static ObservableList<Hermano> data;
    private Hermano hermano;
    public static Hermano hermanoSeleccionado;
    ImportacionDatos importDatos = new ImportacionDatos();
//    StackPane root = new StackPane();
//    GridPane root;
    public static StackPane root;

    @Override
    public void start(Stage primaryStage) {

        importDatos.importacion();
        // meter por parametro lista individuos
        data = FXCollections.observableArrayList(importDatos.getListaHermanos());

        // mostar pantalla datos del array paso 1
        root = new StackPane();
        Scene scene = new Scene(root, 700, 700);
        HBox hBox = new HBox();
        VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(table);
        root.getChildren().add(vbox);
//        root.getChildren().add(hBox);
        Button botonAñadir = new Button("Añadir");
        Button botonEditar = new Button("Editar");
        Button botonSuprimir = new Button("Borrar");
        hBox.getChildren().add(botonAñadir);
        hBox.getChildren().add(botonEditar);
        hBox.getChildren().add(botonSuprimir);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(20);

        TableColumn NombreColumna = new TableColumn("Nombre");
        NombreColumna.setMinWidth(200);
        NombreColumna.setCellValueFactory(new PropertyValueFactory<Hermano, String>("nombre"));

        TableColumn apellidosColumna = new TableColumn("Apellidos");
        apellidosColumna.setMinWidth(200);
        apellidosColumna.setCellValueFactory(new PropertyValueFactory<Hermano, String>("apellidos"));

        //Cambiar Fecha a tipo date y darle formate
        TableColumn fechaColumna = new TableColumn("Fecha");
        fechaColumna.setMinWidth(200);
        fechaColumna.setCellValueFactory(new PropertyValueFactory<Hermano, String>("fecha"));
        setDateFormatColumn(fechaColumna, "dd/MM/yyyy");

        data = FXCollections.observableArrayList(ImportacionDatos.listaHermanos);
        table.setItems(data);
        table.getColumns().addAll(NombreColumna, apellidosColumna, fechaColumna);

        primaryStage.setTitle("Censo de Hermanos");
        primaryStage.setScene(scene);
        primaryStage.show();

        botonAñadir.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                mostrarPantalla0Controller();
                LOG.finest("Se ha pulsado el botón añadir");
            }
        });
        botonEditar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                mostrarPantalla0Controller();
                LOG.finest("Se ha pulsado el botón editar");
            }
        });

        botonSuprimir.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ImportacionDatos.listaHermanos.remove(hermanoSeleccionado);
                data.remove(hermanoSeleccionado);
                LOG.finest("Se ha borrado al pulsar el boton suprimir");
                LOG.finest(String.valueOf(ImportacionDatos.listaHermanos.size()));
                Hermano h = table.getSelectionModel().getSelectedItem();
                if (h != null) {
                    System.out.println(h.getNombre());
                    ImportacionDatos.listaHermanos.remove(h);
                    data.remove(h);
                    System.out.println("Elemento borrado");
                } else {
                    System.out.println("Ningún elemento seleccionado");
                }

            }
        });

        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
                mostrarPantalla0Controller();
                hermanoSeleccionado = table.getSelectionModel().getSelectedItem();
                LOG.finest("Se ha seleccionado un hermano");
            }
        });
        //     generamos el archivo XML
        importDatos.getXML();
        // cargamos la lista de los individuos a partir de un documento XML
        importDatos.getHermanosConXML();
        // cargamos el XML desde una URL de un servidor donde esta alojado
        importDatos.getHermanosURL();

    }

    public static void mostrarHermanos(ArrayList<Hermano> listaHermanos) {
        for (Hermano h : listaHermanos) {
            System.out.println(h.getNombre() + " " + h.getApellidos());

        }
    }

    private void setDateFormatColumn(TableColumn dateColumn, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        dateColumn.setCellFactory(myDateTableCell -> {
            return new TableCell<Object, Date>() {
                @Override
                protected void updateItem(Date date, boolean dateIsEmpty) {
                    super.updateItem(date, dateIsEmpty);
                    if (date == null || dateIsEmpty) {
                        setText(null);
                    } else {
                        setText(simpleDateFormat.format(date));
                    }
                }
            };
        });
    }

    private void mostrarPantalla0Controller() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("pantalla0.fxml"));
            AnchorPane pantalla0 = loader.load();
            Pantalla0Controller pantalla0Controller = loader.getController();
            pantalla0Controller.setListaHermanos(this);
            // Pasar una referencia a la tabla para que pantalla0 pueda acceder a su contenido
            pantalla0Controller.setTableView(table);
            pantalla0Controller.showItemSelectedData();
            root.getChildren().add(pantalla0);
        } catch (IOException ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public static void main(String[] args) {
//        ImportacionDatos.importacion();
        launch(args);
    }

}
