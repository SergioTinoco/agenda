/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.util.Date;

/**
 *
 * @author Sergio Tinoco
 */
public class Hermano {

    // crear un entero (id)
    private int id;
    private String nombre;
    private String apellidos;
    private String estadCivil;
    private String dni;
    private String letraDni;
    private String nombreCalle;
    private String codPostal;
    private String domicilio;
    private String telefono;
    private String profesion;
    private Date fechNacimiento;
    private String ciudadNacimento;
    private String provinciaNacimiento;
    private String parroquia;
    private String ciudadParroquia;
    private String provinciaParroquia;
    private String provinciaHermano;
    private String num;
    private String piso;
    private String email;

    private String movil;

    public Hermano(int id, String nombre, String apellidos, String estadCivil, String dni, String letraDni, String nombreCalle, String codPostal, String domicilio, String telefono, String profesion, Date fechNacimiento, String ciudadNacimento, String provinciaNacimiento, String parroquia, String ciudadParroquia, String provinciaParroquia, String provinciaHermano, String num, String piso, String email) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.estadCivil = estadCivil;
        this.dni = dni;
        this.letraDni = letraDni;
        this.nombreCalle = nombreCalle;
        this.codPostal = codPostal;
        this.domicilio = domicilio;
        this.telefono = telefono;
        this.profesion = profesion;
        this.fechNacimiento = fechNacimiento;
        this.ciudadNacimento = ciudadNacimento;
        this.provinciaNacimiento = provinciaNacimiento;
        this.parroquia = parroquia;
        this.ciudadParroquia = ciudadParroquia;
        this.provinciaParroquia = provinciaParroquia;
        this.provinciaHermano = provinciaHermano;
        this.num = num;
        this.piso = piso;
        this.email = email;
    }

    public Hermano() {
    }

    public String getLetraDni() {
        return letraDni;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public void setLetraDni(String letraDni) {
        this.letraDni = letraDni;
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public void setNombreCalle(String nombreCalle) {
        this.nombreCalle = nombreCalle;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public Date getFechNacimiento() {
        return fechNacimiento;
    }

    public void setFechNacimiento(Date fechNacimiento) {
        this.fechNacimiento = fechNacimiento;
    }

    public String getCiudadNacimento() {
        return ciudadNacimento;
    }

    public void setCiudadNacimento(String ciudadNacimento) {
        this.ciudadNacimento = ciudadNacimento;
    }

    public String getProvinciaNacimiento() {
        return provinciaNacimiento;
    }

    public void setProvinciaNacimiento(String provinciaNacimiento) {
        this.provinciaNacimiento = provinciaNacimiento;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getCiudadParroquia() {
        return ciudadParroquia;
    }

    public void setCiudadParroquia(String ciudadParroquia) {
        this.ciudadParroquia = ciudadParroquia;
    }

    public String getProvinciaParroquia() {
        return provinciaParroquia;
    }

    public void setProvinciaParroquia(String provinciaParroquia) {
        this.provinciaParroquia = provinciaParroquia;
    }

    public String getProvinciaHermano() {
        return provinciaHermano;
    }

    public void setProvinciaHermano(String provinciaHermano) {
        this.provinciaHermano = provinciaHermano;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEstadCivil() {
        return estadCivil;
    }

    public void setEstadCivil(String estadCivil) {
        this.estadCivil = estadCivil;
    }

    public String getCodigPostal() {
        return codPostal;
    }

    public void setCodigPostal(String codigPostal) {
        this.codPostal = codigPostal;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCalle() {
        return nombreCalle;
    }

    public void setCalle(String calle) {
        this.nombreCalle = calle;
    }

    public String getOdigoPostal() {
        return codPostal;
    }

    public void setOdigoPostal(String odigoPostal) {
        this.codPostal = odigoPostal;
    }

    public String getMunicipio() {
        return domicilio;
    }

    public void setMunicipio(String municipio) {
        this.domicilio = municipio;
    }

    public String getTelefono1() {
        return telefono;
    }

    public void setTelefono1(String telefono1) {
        this.telefono = telefono1;
    }

    public String getTelefono2() {
        return profesion;
    }

    public void setTelefono2(String telefono2) {
        this.profesion = telefono2;
    }

    public Date getFecha() {
        return fechNacimiento;
    }

    public void setFecha(Date fecha) {
        this.fechNacimiento = fecha;
    }

    boolean getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
