/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author SergioTinoco
 */
public class ImportacionDatos {

    private static final Logger LOG = Logger.getLogger(ImportacionDatos.class.getName());
    public static ArrayList<Hermano> listaHermanos;
    public static Hermano hermano;
    public static Hermanos hermanos = new Hermanos();

    public void importacion() {
        String nombreFichero = "censoHermanos.csv";
        //Declarar una variable BufferedReader
        BufferedReader br = null;
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/mm/yy");
        try {
            //Crear un objeto BufferedReader al que se le pasa
            //   un objeto FileReader con el nombre del fichero
            br = new BufferedReader(new FileReader(nombreFichero));
            //Leer la primera línea, guardando en un String
            String texto = br.readLine();
            //Repetir mientras no se llegue al final del fichero
            listaHermanos = new ArrayList();
            while (texto != null) {
                System.out.println(texto);
                String[] data = texto.split(";");
                Date fecha = null;

                try {
                    fecha = formatoFecha.parse(data[10]);

                } catch (ParseException ex) {
                    System.out.println("No se ha podido convertir la fecha correctamente");
                    System.out.println(ex.getMessage());

                }

                hermano = new Hermano(0, data[0], data[1], data[2], data[3], data[4],
                        data[5], data[6], data[7], data[8], data[9], fecha, data[11], data[12], data[13],
                        data[14], data[15], data[16], data[17], data[18], data[19]);
                listaHermanos.add(hermano);
                hermanos.getListaHermanos().add(hermano);
                LOG.fine("tamaño: " + listaHermanos.size());
                LOG.fine(String.valueOf(data[0] + " " + data[4]));
                //Leer la siguiente línea
                texto = br.readLine();
            }

            LOG.fine(String.valueOf(listaHermanos.size()));
        } catch (FileNotFoundException e) {
            LOG.finest("El Fichero no se ha encontrado");
            LOG.finest(e.getMessage());
        } catch (Exception e) {
            LOG.finest("Error de lectura del fichero");
            LOG.finest(e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                LOG.finest("Error al cerrar el fichero");
                LOG.finest(e.getMessage());
            }
        }
    }

    public static ArrayList<Hermano> getListaHermanos() {
        return listaHermanos;
    }

    public void getXML() {
        try {
            // Se indica el nombre de la clase que contiene la lista de objetos
            JAXBContext jaxbContext = JAXBContext.newInstance(Hermanos.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // Indicar que se desea generar el xml con saltos de línea y tabuladores
            //  para facilitar su lectura
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Generar XML mostrándolo por la salida estándar
            jaxbMarshaller.marshal(hermanos, System.out);

            // Generar XML guardándolo en un archivo local
            BufferedWriter bw = new BufferedWriter(new FileWriter("hermanos.xml"));
            jaxbMarshaller.marshal(hermanos, bw);
        } catch (JAXBException ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getHermanosConXML() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("hermanos.xml"));
            JAXBContext jaxbContext = JAXBContext.newInstance(Hermanos.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            hermanos = (Hermanos) jaxbUnmarshaller.unmarshal(br);
            for (Hermano hermano : hermanos.getListaHermanos()) {
                System.out.println("Nombre: " + hermano.getNombre());
                System.out.println("Apellidos: " + hermano.getApellidos());
                System.out.println("Dni: " + hermano.getDni());

            }
        } catch (JAXBException ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getHermanosURL() {
        try {
            // Crear objeto JAXB para interpretar objetos 'Items' desde XML
            JAXBContext jaxbContext = JAXBContext.newInstance(Hermanos.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            // Generar lista de objetos desde XML descargado de URL
            URL url = new URL("http://sergiotinoco.esy.es/hermanos.xml");
            InputStream is = url.openStream();
            Hermanos hermanos = (Hermanos) jaxbUnmarshaller.unmarshal(is);

            // Mostrar el contenido de la lista obtenida
            System.out.println("Hermanos desde URL");
            for (Hermano hermano : hermanos.getListaHermanos()) {
                System.out.println("Nombre: " + hermano.getNombre());
                System.out.println("Apellidos: " + hermano.getApellidos());
                System.out.println("Dni: " + hermano.getDni());
            }
        } catch (Exception ex) {
            Logger.getLogger(CensoHermanos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
